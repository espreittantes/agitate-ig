import Config

config :agita_ig,
  input_file: "poi",
  file_ext: ".csv",
  data_dir: "priv"
