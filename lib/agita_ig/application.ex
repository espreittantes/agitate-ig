defmodule AgitaIG.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    # Starts workers by calling: AgitaIG.Worker.start_link(arg)
    children =
      AgitaIG.RegisterIG.data_source()
      |> File.read!()
      |> String.split()
      |> Enum.map(fn account ->
        Supervisor.child_spec({AgitaIG.SpiderIG, account}, id: String.to_atom(account))
      end)

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: AgitaIG.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
