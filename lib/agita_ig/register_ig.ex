defmodule AgitaIG.RegisterIG do
  @moduledoc """
  Documentation for `RegisterIG`, a module to output IG data to file.
  """

  require Logger

  @source_file Application.compile_env!(:agita_ig, :input_file)
  @data_dir Application.compile_env!(:agita_ig, :data_dir)
  @file_ext Application.compile_env!(:agita_ig, :file_ext)

  @spec data_source() :: binary()
  def data_source(), do: Path.join([@data_dir, @source_file <> @file_ext])

  @spec read(String.t()) :: {:ok, integer()}
  def read(name) do
    path = Path.join([@data_dir, name <> @file_ext])

    if File.exists?(path) do
      # we assume one line per IG post
      nlines =
        path
        |> File.read!()
        |> String.split("\n")
        |> Enum.count()

      Logger.debug("Found #{nlines - 1} saved posts for #{name}")
      {:ok, nlines - 1}
    else
      Logger.debug("No previous data found for #{name}")
      {:ok, 0}
    end
  end

  @spec write(String.t(), integer(), map(), list(map())) :: :ok
  def write(name, newposts, _userdata, _postdata) do
    Logger.debug("Saving #{newposts} new posts for #{name}")
    f = Path.join([@data_dir, name <> @file_ext]) |> File.open!([:append])
    # TODO: replace line below by writing the relevant IG data!
    1..newposts |> Enum.each(fn i -> IO.write(f, "post #{i}\n") end)
    File.close(f)
  end
end
