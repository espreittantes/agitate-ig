defmodule AgitaIG.SpiderIG do
  @moduledoc """
  Documentation for `SpiderIG`, a module to extract information from an IG account.
  It works as a GenServer, so multiple instances (for multiple IG accounts) can be spawned.
  """

  use GenServer, restart: :transient

  # we timeout to large amounts voluntarily, for honest, non-invasive crawling
  @request_interval 100_000
  @type account :: %{
          username: String.t(),
          nposts: integer(),
          userdata: map(),
          posts: list(map())
        }

  require Logger

  # required by Application
  @spec start_link(term()) :: {:ok, pid()}
  def start_link(arg), do: GenServer.start_link(__MODULE__, arg)

  # client API so that data gathering starts when externally indicated
  @spec crawl() :: :ok
  def crawl(), do: GenServer.cast(__MODULE__, :crawl)

  # Callbacks

  @impl true
  def init(account_name) do
    Logger.debug("Starting IG spider for #{account_name}")
    # if there's a file, then we should the data as starting point
    {:ok, nposts} = AgitaIG.RegisterIG.read(account_name)
    {:ok, %{username: account_name, nposts: nposts}}
  end

  @impl true
  def handle_cast(:crawl, state) do
    Logger.debug("Asynchronously starting crawling process for #{state.username}")
    schedule_perform_request()
    {:noreply, state}
  end

  @impl true
  def handle_info(:do_crawl, state) do
    Logger.debug("Asynchronously spawning new request task")
    Task.async(AgitaIG.InformantIG, :request, [state.username])
    {:noreply, state}
  end

  def handle_info({task, {user, posts}}, state) when is_reference(task) do
    Logger.debug(
      "Asynchronously processing request task results: user #{user["full_name"]} has published #{posts["count"]} IG posts"
    )

    Process.demonitor(task, [:flush])
    schedule_perform_request()

    if state.nposts == posts["count"] do
      Logger.debug("User #{user["full_name"]} has no new posts, stopping")
      {:stop, :normal, state}
    else
      diff_no_posts = posts["count"] - state.nposts
      Logger.debug("User #{user["full_name"]} has #{diff_no_posts} new posts")

      new_state =
        Map.replace!(state, :nposts, posts["count"])
        |> Map.put(:userdata, user)
        |> Map.put(:posts, posts["edges"])

      Task.start(AgitaIG.RegisterIG, :write, [
        new_state.username,
        diff_no_posts,
        new_state.userdata,
        new_state.posts
      ])

      {:noreply, new_state}
    end
  end

  # Internal functions

  @spec schedule_perform_request() :: reference()
  defp schedule_perform_request() do
    timeout = trunc(:rand.uniform() * @request_interval)
    Logger.debug("Scheduling request for #{self()} in #{timeout} milliseconds")

    Process.send_after(
      self(),
      :do_crawl,
      timeout
    )
  end
end
