defmodule AgitaIG.InformantIG do
  @moduledoc """
  Documentation for `InformantIG`, a module to request and format IG data.
  """

  require Logger

  @ig_api_url "https://i.instagram.com/api/v1/users/web_profile_info/?username="
  @ig_http_header_field "x-ig-app-id"
  @ig_app_id 936_619_743_392_459

  @doc """
  Requests IG data for a given username and returns some data.
  """
  def request(username) do
    middleware = [
      {Tesla.Middleware.BaseUrl, @ig_api_url},
      {Tesla.Middleware.Headers, [{@ig_http_header_field, @ig_app_id}]},
      Tesla.Middleware.JSON
    ]

    request = @ig_api_url <> username
    Logger.debug("Performing request #{request}")
    client = Tesla.client(middleware)
    {:ok, response} = Tesla.get(client, request)

    response.body |> extract()
  end

  defp extract(data) do
    if data["status"] == "ok" do
      user = data["data"]["user"]
      posts = data["data"]["user"]["edge_owner_to_timeline_media"]
      {user, posts}
    end
  end
end
