defmodule AgitaIG do
  @moduledoc """
  Documentation for `AgitaIG`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> AgitaIG.hello()
      :world

  """
  def hello do
    :world
  end
end
