# AGITA-IG: analysis tool for the Spanish political IG-sphere

## Description

As part of the [AGITATE](https://agitate.udc.es/) project, we develop this tool, to extract relevant information from Instagram.

## Installation

This tool is implemented in [Elixir](https://elixir-lang.org). Since the project is not distributed using containers 
at the moment, you will need a working Elixir installation to run it. The following instructions are compatible with Debian/Ubuntu
as operating system, which is our recommended setting.

To install the most recent Elixir version supported by your Linux distribution, plus the needed support for building the project,
use:

```apt install elixir erlang-dev```

Upon successful installation, clone this repository, switch to the corresponding directory, and proceed to download project
dependencies with:

```mix deps.get```

The first time, this will prompt the installation of [Hex](https://hex.pm/), Elixir's package manager:

```
Could not find Hex, which is needed to build dependency :credo
Shall I install Hex? (if running non-interactively, use "mix local.hex --force") [Yn]
```

Do select `Y' or just press enter to proceed.

Upon successful download of all dependencies, proceed to compile the project with:

```mix compile```

The first time, this will prompt the installation of [rebar3](https://rebar3.org/), Erlang/Elixir building tool:

```
Could not find "rebar3", which is needed to build dependency :unicode_util_compat
I can install a local copy which is just used by Mix
Shall I install rebar3? (if running non-interactively, use "mix local.rebar --force") [Yn]
```

Again, do select `Y' or just press enter to complete the installation process.

## Usage

TODO.

## Contributing & Support

If you need assistance, feel free to open an issue on this repo. 

If you want to contribute to this project, PRs are welcome!

## Project status & Roadmap

TODO.

## Acknowledgments

TODO.

## License

This project is licensed under GNU GPLv3.
